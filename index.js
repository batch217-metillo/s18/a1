// console.log("Hello World!");
	
	/*1. Create a function which will be able to add two numbers.
			-Numbers must be provided as arguments.
			-Display the result of the addition in our console.
			-function should only display result. It should not return anything.

		Create a function which will be able to subtract two numbers.
			-Numbers must be provided as arguments.
			-Display the result of subtraction in our console.
			-function should only display result. It should not return anything.

			-invoke and pass 2 arguments to the addition function
			-invoke and pass 2 arguments to the subtraction function*/


	// Code for instruction #1

		function printSum(num1, num2) {
			let sum = num1 + num2;
			console.log("Displayed sum of " + num1 + " and " + num2);
			console.log(sum);
		}

		printSum(5, 15);

		function printDifference(num3, num4) {
			let difference = num3 - num4;
			console.log("Displayed difference of " + num3 + " and " + num4);
			console.log(difference);
		}

		printDifference(20, 5);


	/*2. Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments. 
			-Return the result of the multiplication.

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division.

	 	Create a global variable outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.*/


	// Code for instruction #2

		function returnProduct(num5, num6) {
			let productValue = num5 * num6;
			console.log("The product of " + num5 + " and " + num6 + ":");
			return productValue;
		}

		let product = returnProduct(50, 10);
		console.log(product);

		function returnQuotient(num7, num8) {
			let quotientValue = num7 / num8;
			console.log("The quotient of " + num7 + " and " + num8 + ":");
			return quotientValue;
		}

		let quotient = returnQuotient(50, 10);
		console.log(quotient);


	/*3. Create a function which will be able to get total area of a circle from a provided radius.
			-a number should be provided as an argument.
			-look up the formula for calculating the area of a circle with a provided/given radius.
			-look up the use of the exponent operator.
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation.

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

		Log the value of the circleArea variable in the console.*/


	// Code for instruction #3

		function returnArea(pi, rad) {
			let areaOfCircle = pi * (rad**2);
			console.log("The result of getting the area of a circle with " + rad + " radius:");
			return areaOfCircle;
		}

		let circleArea = returnArea(Math.PI, 15);
		console.log(circleArea.toFixed(2));


	/*4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-look up the formula for calculating the average of numbers.
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation.

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.*/


	// Code for instruction #4

		function returnAverage(n1, n2, n3, n4) {
			let average = (n1 + n2 + n3 + n4) / 4;
			console.log("The average of " + n1 + "," + n2 + "," + n3 + " and " + n4 + ":");
			return average;
		}
		
		let averageVar = returnAverage(20, 40, 60, 80);
		console.log(averageVar);


	/*5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.*/


		// Code for instruction #5

		function returnPercentage(myScore, totalScore) {
			let myAverage = (myScore/totalScore) * 100;
			let isPassed = myAverage > 75;
			console.log("Is " + myScore + "/" + totalScore + " a passing score?");
			return isPassed;
		}

		let isPassingScore = returnPercentage(38, 50);
		console.log(isPassingScore);